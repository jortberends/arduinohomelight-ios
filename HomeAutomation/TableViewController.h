//
//  TableViewController.h
//  HomeAutomation
//
//  Created by Jort Berends on 05/03/15.
//  Copyright (c) 2015 Appetite BV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "JBConnector.h"

@interface TableViewController : UITableViewController <CLLocationManagerDelegate>{
    NSURLConnection *connection;
    NSData *receivedData;
}
@end
