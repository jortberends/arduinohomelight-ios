//
//  JBPlug.h
//  HomeAutomation
//
//  Created by Jort Berends on 11/09/15.
//  Copyright (c) 2015 Appetite BV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JBPlug : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSInteger identifier;
@property (nonatomic, assign) BOOL on;

@end
