//
//  AppDelegate.m
//  HomeAutomation
//
//  Created by Jort Berends on 05/03/15.
//  Copyright (c) 2015 Appetite BV. All rights reserved.
//

#import "AppDelegate.h"

#import "JBConnector.h"

@interface AppDelegate ()

@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    
    [self registerShortcutItems];
    [self registerLocalNotifications];
    
    UIApplicationShortcutItem *shortcutItem = [launchOptions objectForKey:UIApplicationLaunchOptionsShortcutItemKey];
    if(shortcutItem){
        [self handleShortCutItem:shortcutItem];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler {
    [self handleShortCutItem:shortcutItem];
}

#pragma mark - Custom methods

- (void) registerShortcutItems
{
    UIApplicationShortcutIcon * powerOffIcon = [UIApplicationShortcutIcon iconWithTemplateImageName: @"PowerOff"]; // your customize icon
    UIApplicationShortcutItem * powerOffItem = [[UIApplicationShortcutItem alloc]initWithType: @"master-off" localizedTitle: @"Alles uit" localizedSubtitle: nil icon: powerOffIcon userInfo: nil];
    
    [UIApplication sharedApplication].shortcutItems = @[powerOffItem];
}

- (void)handleShortCutItem:(UIApplicationShortcutItem *)shortcutItem  {
    if([shortcutItem.type isEqualToString:@"master-off"]){
        [[JBConnector sharedConnector] setSwitch:4 value:NO];
    }
}

- (void)registerLocalNotifications
{
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:
                                            (UIUserNotificationTypeBadge
                                             |UIUserNotificationTypeSound
                                             |UIUserNotificationTypeAlert
                                             ) categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
}


#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region
{
    NSLog(@"didEnterRegion");
    [[JBConnector sharedConnector] setSwitch:5 value:YES];
    
    UILocalNotification *localNotification = [UILocalNotification new];
    localNotification.alertBody = @"Ik heb je lampen vast aangezet.";
    localNotification.soundName = @"Default";
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
}

- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region
{
    NSLog(@"didExitRegion");
    [[JBConnector sharedConnector] setSwitch:5 value:NO];
    
    UILocalNotification *localNotification = [UILocalNotification new];
    localNotification.alertBody = @"Ik heb al je lampen uitgezet.";
    localNotification.soundName = @"Default";
    [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
}

@end
