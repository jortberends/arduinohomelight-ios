//
//  AppDelegate.h
//  HomeAutomation
//
//  Created by Jort Berends on 05/03/15.
//  Copyright (c) 2015 Appetite BV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

