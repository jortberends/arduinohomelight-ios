//
//  main.m
//  HomeAutomation
//
//  Created by Jort Berends on 05/03/15.
//  Copyright (c) 2015 Appetite BV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
