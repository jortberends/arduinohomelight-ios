//
//  JBConnector.m
//  HomeAutomation
//
//  Created by Jort Berends on 09/03/15.
//  Copyright (c) 2015 Appetite BV. All rights reserved.
//

#import "JBConnector.h"

static JBConnector * _sharedConnector = nil;

@implementation JBConnector

#pragma mark - Singleton access
+ (JBConnector *) sharedConnector {
    @synchronized([JBConnector class]) {
        if(!_sharedConnector) {
            _sharedConnector = [[JBConnector alloc] init];
        }
        return _sharedConnector;
    }
    return nil;
}

- (void) mqttAction:(void (^)(void))completion
{
    if (_client == nil)
    {
        _client = [[MQTTClient alloc] initWithClientId:[self deviceUUID]];
        _client.password = @"JNMkZtAV5AcG";
        _client.username = @"nlurkitr";
        _client.port = 13996;        
        
        [_client setMessageHandler:^(MQTTMessage *message) {
            NSString *text = message.payloadString;
            NSLog(@"received message %@", text);
            
            NSError *error = nil;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:message.payload options:0 error:&error];
            
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"PlugAvailable" object:parsedObject];
        }];
    }

    
    if (!_client.connected)
    {
        // connect the MQTT client
        [_client connectToHost:@"m20.cloudmqtt.com" completionHandler:^(MQTTConnectionReturnCode code) {
            if (code == ConnectionAccepted) {
                
                if (completion)
                {
                    completion();
                }
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"JBConnectorConnected" object:nil];
            }
        }];
    }
    else
    {
        if (completion)
        {
            completion();
        }
    }
}

- (void) subscribe
{
    NSLog(@"subscribing");

    [self mqttAction:^{
        [_client subscribe:@"lights" withCompletionHandler:^(NSArray *array){
            NSLog(@"Subscribed");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"JBConnectorSubscribed" object:nil];
        }];
    }];
}

-(void) setSwitch:(NSInteger)identifier value:(BOOL)value
{
    
    if (value)
    {
        NSLog(@"Ga aan");
    }
    else
    {
        NSLog(@"Ga uit");
    }
    
    
    NSString *message = [NSString stringWithFormat:@"{\"plug\":%ld,\"on\":%@}",(long)identifier,value ? @"true" : @"false"];
    
    [self mqttAction:^{
        [_client publishString:message toTopic:@"lights" withQos:AtMostOnce retain:NO completionHandler:nil];
    }];
}

-(void)updatePlugs
{
    [self mqttAction:^{
        NSLog(@"get status");
        [_client publishString:@"{\"status\":true}" toTopic:@"lights" withQos:AtMostOnce retain:NO completionHandler:nil];
    }];
    
}

/**
 *  http://stackoverflow.com/a/20944977
 **/
- (NSString *)deviceUUID
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:[[NSBundle mainBundle] bundleIdentifier]])
        return [[NSUserDefaults standardUserDefaults] objectForKey:[[NSBundle mainBundle] bundleIdentifier]];
    
    @autoreleasepool {
        
        CFUUIDRef uuidReference = CFUUIDCreate(nil);
        CFStringRef stringReference = CFUUIDCreateString(nil, uuidReference);
        NSString *uuidString = (__bridge NSString *)(stringReference);
        [[NSUserDefaults standardUserDefaults] setObject:uuidString forKey:[[NSBundle mainBundle] bundleIdentifier]];
        [[NSUserDefaults standardUserDefaults] synchronize];
        CFRelease(uuidReference);
        CFRelease(stringReference);
        return uuidString;
    }
}

@end
