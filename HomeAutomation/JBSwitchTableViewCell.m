//
//  JBSwitchTableViewCell.m
//  HomeAutomation
//
//  Created by Jort Berends on 11/09/15.
//  Copyright (c) 2015 Appetite BV. All rights reserved.
//

#import "JBSwitchTableViewCell.h"

@implementation JBSwitchTableViewCell

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void) setSwitchOn:(BOOL)on animated:(BOOL)animated
{
    [self.onOffSwitch setOn:on animated:animated];
}

- (IBAction)switchChanged:(id)sender
{    
    [[JBConnector sharedConnector] setSwitch:self.plug.identifier value:[sender isOn]];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    if (editing == NO)
    {
        self.descriptionLabel.text = self.plug.name = self.descriptionInput.text;
        [self.descriptionInput resignFirstResponder];
    }
    
    self.descriptionInput.hidden = NO;
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        self.onOffSwitch.alpha = !editing;
        self.descriptionLabel.alpha = !editing;
        
        self.descriptionInput.alpha = editing;
        
    } completion:nil];
}

- (void) setPlug:(JBPlug *)plug
{
    _plug = plug;
    _descriptionLabel.text = plug.name;
    _onOffSwitch.on = plug.on;
    _descriptionInput.text = plug.name;
}

@end
