//
//  JBSwitchTableViewCell.h
//  HomeAutomation
//
//  Created by Jort Berends on 11/09/15.
//  Copyright (c) 2015 Appetite BV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBConnector.h"
#import "JBPlug.h"

@interface JBSwitchTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UISwitch *onOffSwitch;
@property (nonatomic, weak) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, weak) IBOutlet UITextField *descriptionInput;

@property (nonatomic, weak) JBPlug *plug;

- (IBAction)switchChanged:(id)sender;
- (void) setSwitchOn:(BOOL)on animated:(BOOL)animated;
- (void) setPlug:(JBPlug *)plug;
@end
