//
//  JBPlug.m
//  HomeAutomation
//
//  Created by Jort Berends on 11/09/15.
//  Copyright (c) 2015 Appetite BV. All rights reserved.
//

#import "JBPlug.h"

@implementation JBPlug

- (void)encodeWithCoder:(NSCoder *)coder;
{
    [coder encodeObject:self.name forKey:@"name"];
    [coder encodeInteger:self.identifier forKey:@"identifier"];
    [coder encodeBool:self.on forKey:@"on"];
}

- (id)initWithCoder:(NSCoder *)coder;
{
    self = [super init];
    if (self != nil)
    {
        self.name = [coder decodeObjectForKey:@"name"];
        self.identifier = [coder decodeIntegerForKey:@"identifier"];
        self.on  = [coder decodeBoolForKey:@"on"];
    }
    return self;
}

@end
