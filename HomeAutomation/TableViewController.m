//
//  TableViewController.m
//  HomeAutomation
//
//  Created by Jort Berends on 05/03/15.
//  Copyright (c) 2015 Appetite BV. All rights reserved.
//

#import "TableViewController.h"
#import "JBSwitchTableViewCell.h"
#import "JBPlug.h"


@interface TableViewController ()

@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Home Light";
    
    self.tableView.allowsSelection = NO;
    
    self.dataSource = [[NSMutableArray alloc] init];
    
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    NSData *dataRepresentingSavedArray = [currentDefaults objectForKey:@"savedPlugs"];
    if (dataRepresentingSavedArray != nil)
    {
        NSArray *oldSavedArray = [NSKeyedUnarchiver unarchiveObjectWithData:dataRepresentingSavedArray];
        if (oldSavedArray != nil)
        {
            self.dataSource = [[NSMutableArray alloc] initWithArray:oldSavedArray];
        }
        else
        {
            self.dataSource = [[NSMutableArray alloc] init];
        }
    }
    
    if ([self.dataSource count] == 0)
    {
        JBPlug *plug = [[JBPlug alloc] init];
        plug.identifier = 4;
        plug.name = @"Master";
        plug.on = NO;
        [self.dataSource addObject:plug];
    }
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventValueChanged];
    
    //  Register for notifications
     [[NSNotificationCenter defaultCenter]
    addObserver:self selector:@selector(plugAvailable:) name:@"PlugAvailable" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectorSubscribed:) name:@"JBConnectorSubscribed" object:nil];
    
    //  Connect and subscribe
    [[JBConnector sharedConnector] subscribe];
    //[self refreshData];
    
    self.locationManager = [CLLocationManager new];
    [self setupGeofencing];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.dataSource count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    JBSwitchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JBSwitchTableViewCell"];
    
    if (!cell)
    {
        cell = [[JBSwitchTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"JBSwitchTableViewCell"];
    }
    
    JBPlug *plug = [self.dataSource objectAtIndex:indexPath.row];
    cell.plug = plug;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}

- (void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    JBPlug *plug = [self.dataSource objectAtIndex:[sourceIndexPath row]];
    [self.dataSource removeObjectAtIndex:[sourceIndexPath row]];
    [self.dataSource insertObject:plug atIndex:[destinationIndexPath row]];
}

- (void) connectorSubscribed:(NSNotification *)notification
{
    //  Get connectors when subscribed
    [self refreshData];
}

-(void) plugAvailable:(NSNotification *)notification
{
    if([notification.object isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *plugData = [notification object];
        
        if(![plugData valueForKey:@"status"]){
        
            NSNumber *onOff = [plugData valueForKey:@"on"];
        
            NSInteger identifier = [[plugData valueForKey:@"plug"] integerValue];
        
            [[NSOperationQueue mainQueue] addOperationWithBlock:^ {
                
                BOOL plugFound = NO;
                
                for (JBPlug *plug in self.dataSource)
                {
                    if (plug.identifier == identifier)
                    {
                        [plug setOn:onOff.boolValue];
                        plugFound = YES;
                    }
                }
                
                if (!plugFound)
                {
                    JBPlug *plug = [[JBPlug alloc] init];
                    plug.on = onOff.boolValue;
                    plug.identifier = identifier;
                    plug.name = [NSString stringWithFormat:@"Plug: %ld",(long)identifier];
                    [self.dataSource addObject:plug];
                }
                
                [self.tableView reloadData];
             
                [self.refreshControl endRefreshing];
            }];
        }
    }
}

- (void) handleRefreshDataFailure
{
    if (self.refreshControl.isRefreshing)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Offline" message:@"De lampen zijn offline.\nOf jij bent offline.\nOf jij zit achter een proxy." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        
        [self.refreshControl endRefreshing];
    }
}

-(void) refreshData{
    
    [[JBConnector sharedConnector] updatePlugs];
    [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(handleRefreshDataFailure) userInfo:nil repeats:NO];
    
    NSLog(@"Reload Data");
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    if (!editing)
    {
        [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:self.dataSource] forKey:@"savedPlugs"];
    }
}

#pragma mark - Notifications
- (void) registerForLocalNotifications
{
    UIUserNotificationType notificationTypes = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:notificationTypes categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
}

#pragma mark - Geofencing
- (void) setupGeofencing
{
    NSLog(@"setupGeofencing");
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            [self askForPermission];
            return;
            
        case kCLAuthorizationStatusAuthorizedAlways:
            [self setUpGeofences];
            return;
            
        case kCLAuthorizationStatusDenied:
        case kCLAuthorizationStatusRestricted:
            [self showSorryAlert];
            return;
    }
}

- (void)askForPermission
{
    NSLog(@"askForPermission");
    if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    } else {
        [self setUpGeofences];
    }
}

- (void) setUpGeofences
{
    NSLog(@"setUpGeofences");
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(52.0927,5.1242);
    CLRegion *home = [[CLCircularRegion alloc]initWithCenter:center
                                                      radius:100.0
                                                  identifier:@"Home"];
    [self.locationManager startMonitoringForRegion:home];
}

- (void)showSorryAlert
{
    NSLog(@"showSorryAlert");
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    NSLog(@"didChangeAuthorizationStatus");
}

- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region
{
    NSLog(@"didStartMonitoringForRegion");
}

- (void)locationManager:(CLLocationManager *)manager monitoringDidFailForRegion:(CLRegion *)region withError:(NSError *)error
{
    NSLog(@"monitoringDidFailForRegion");
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError");
}

@end
