//
//  JBConnector.h
//  HomeAutomation
//
//  Created by Jort Berends on 09/03/15.
//  Copyright (c) 2015 Appetite BV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MQTTKit.h>

@interface JBConnector : NSObject <NSURLConnectionDelegate> {
    NSURLConnection *_connection;
    NSMutableData *_responseData;
    MQTTClient *_client;
}

+(JBConnector*) sharedConnector;

-(void) setSwitch:(NSInteger)identifier value:(BOOL)value;
-(void) updatePlugs;
-(void) subscribe;

@end
